package com.example.dev.actividadDocker.controller;

import com.example.dev.actividadDocker.model.Producto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/apiProductos")
public class ProductoController {

    private static List<Producto> productos = new ArrayList<>();

    @GetMapping("/productos")
    public List<Producto> getProductos() {

        Producto producto1 = new Producto("Manzana", "frutas", 10, 990, "P2");
        Producto producto2 = new Producto("Arroz", "despensa", 40, 1990, "P6");
        Producto producto3 = new Producto("Tallarines", "despensa", 27, 690, "P6");
        Producto producto4 = new Producto("Papas fritas", "snacks", 19, 2990, "P3");

        productos.add(producto1);
        productos.add(producto2);
        productos.add(producto3);
        productos.add(producto4);

        return productos;
    }
}
