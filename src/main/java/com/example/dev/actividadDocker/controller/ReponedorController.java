package com.example.dev.actividadDocker.controller;

import com.example.dev.actividadDocker.model.Reponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/apiReponedor")
public class ReponedorController {

    private static List<Reponedor> reponedores = new ArrayList<>();


    @GetMapping("/reponedores")
    public List<Reponedor> getProductos() {
        Reponedor reponedor1 = new Reponedor("Pedro", "17283647-1", "pedro123", "frutas");
        Reponedor reponedor2 = new Reponedor("Diego", "21374628-2", "diego123", "snacks");
        Reponedor reponedor3 = new Reponedor("Juan", "197822276-4", "juan123", "despensa");

        reponedores.add(reponedor1);
        reponedores.add(reponedor2);
        reponedores.add(reponedor3);

        return reponedores;
    }


}
