package com.example.dev.actividadDocker.controller;


import com.example.dev.actividadDocker.model.Merma;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/apiMerma")
public class MermaController {

    private static List<Merma> mermas = new ArrayList<>();


    @GetMapping("/mermas")
    public List<Merma> getMermas() {
        Merma merma1 = new Merma("M1", "Vencimiento", 5);
        Merma merma2 = new Merma("M2", "Vencimiento", 5);
        Merma merma3 = new Merma("M3", "Vencimiento", 5);

        mermas.add(merma1);
        mermas.add(merma2);
        mermas.add(merma3);

        return mermas;
    }
}
