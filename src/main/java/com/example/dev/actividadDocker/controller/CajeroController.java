package com.example.dev.actividadDocker.controller;

import com.example.dev.actividadDocker.model.Cajero;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/apiCajeros")
public class CajeroController {

    private static List<Cajero> cajeros = new ArrayList<>();


    @GetMapping("/cajeros")
    public List<Cajero> getProductos() {
        Cajero cajero1 = new Cajero("Alexis", "20239128-4", "alexis123", 3);
        Cajero cajero2 = new Cajero("Matias", "20124128-4", "mati123", 73);
        Cajero cajero3 = new Cajero("Gary", "17829128-4", "gary123", 32);

        cajeros.add(cajero1);
        cajeros.add(cajero2);
        cajeros.add(cajero3);

        return cajeros;
    }

}
