package com.example.dev.actividadDocker.model;

public class Usuario {
    private String nombre;
    private String rut;
    private String password;

    public Usuario(String nombre, String rut, String password) {
        this.nombre = nombre;
        this.rut = rut;
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
