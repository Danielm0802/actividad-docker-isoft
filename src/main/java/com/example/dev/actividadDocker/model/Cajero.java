package com.example.dev.actividadDocker.model;

public class Cajero extends Usuario {
    private int ventas;

    public Cajero(String nombre, String rut, String password, int ventas) {
        super(nombre, rut, password);
        this.ventas = ventas;
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }
}
