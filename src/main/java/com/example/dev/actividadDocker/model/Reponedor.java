package com.example.dev.actividadDocker.model;

public class Reponedor extends Usuario {

    private String seccion;

    public Reponedor(String nombre, String rut, String password, String seccion) {
        super(nombre, rut, password);
        this.seccion = seccion;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }
}
