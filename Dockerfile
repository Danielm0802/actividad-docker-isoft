FROM openjdk:17

VOLUME /tmp

EXPOSE 8080

COPY target/actividadDocker-0.0.1-SNAPSHOT.jar actividad-docker.jar

ENTRYPOINT ["java", "-jar", "actividad-docker.jar"]

#sudo docker build -t demo-container .
#sudo docker run -p 8080:8080 -d demo-container